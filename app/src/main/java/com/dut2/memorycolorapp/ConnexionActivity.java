package com.dut2.memorycolorapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ConnexionActivity extends AppCompatActivity {
    TextView goToConnexion;
    TextView goToRegister;

    EditText mail;
    EditText password;
    Button validateConnexion;
    private FirebaseAuth mAuth;
    String TAG="BDDInfo";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);

        goToConnexion = findViewById(R.id.connexion_textView_goToSignIn);
        goToRegister = findViewById(R.id.connexion_textView_goToSignUp);

        mail = findViewById(R.id.connexion_editText_mail);
        password = findViewById(R.id.connexion_editText_mdp);
        validateConnexion = findViewById(R.id.btn_connexion);

        //Initialisation de notre instance FirebaseAuth
        mAuth = FirebaseAuth.getInstance();

        goToConnexion.setPaintFlags(goToConnexion.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        goToConnexion.setTypeface(null, Typeface.BOLD);


        goToRegister.setPaintFlags(goToConnexion.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        goToRegister.setPaintFlags(goToRegister.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);


        //Ouvre l'activité Inscription
        goToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConnexionActivity.this, InscriptionActivity.class);

                startActivity(intent);

            }
        });

        validateConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Appel fonction de connexion
                signInUser(mail.getText().toString(), password.getText().toString());
            }
        });

    }

    public void signInUser(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //Se passe bien : lancement du menu
                            Intent intent = new Intent(ConnexionActivity.this, MenuActivity.class);
                            startActivity(intent);

                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                        } else {
                            //erreur : message d'erreur
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(ConnexionActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }


}