package com.dut2.memorycolorapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InscriptionActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{



    TextView goToConnexion;
    TextView goToRegister;
    EditText firstName;
    EditText name;
    EditText mail;
    EditText password;
    RadioButton sexe_h,sexe_f;
    ImageButton openCalendar;
    TextView date_result; //affiche la date selectionnée
    Button validateRegister;
    private FirebaseAuth mAuth;
    String TAG="BDDInfo";
    private FirebaseFirestore db;

    RadioGroup group_sexe;
    private String sexe; //Sexe choisi lors de l'inscription
    private String date;//date choisi lors de l'inscription
    private int age; //age calculé à partir de la date de naissance renseignée

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);


        goToConnexion = findViewById(R.id.inscription_textView_goToSignIn);
        goToRegister = findViewById(R.id.inscription_textView_goToSignUp);
        firstName = findViewById(R.id.inscription_editText_firstname);
        name = findViewById(R.id.inscription_editText_name);
        mail = findViewById(R.id.inscription_editText_mail);
        password = findViewById(R.id.inscription_editText_password);
        sexe_h = findViewById(R.id.inscription_radio_h);
        sexe_f = findViewById(R.id.inscription_radio_f);
        openCalendar = findViewById(R.id.inscription_btn_openCalendar);
        date_result = findViewById(R.id.date_result);
        validateRegister = findViewById(R.id.btn_inscription);

        //Initialisation de notre instance FirebaseAuth
        mAuth = FirebaseAuth.getInstance();

        db = FirebaseFirestore.getInstance();

        sexe = "Inconnu"; //Par défaut vaut nul
        group_sexe = findViewById(R.id.inscription_groupe_sexe);


        goToConnexion.setPaintFlags(goToConnexion.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        goToRegister.setTypeface(null, Typeface.BOLD);

        goToRegister.setPaintFlags(goToConnexion.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);

        //Ouvre l'activité Connexion
        goToConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InscriptionActivity.this, ConnexionActivity.class);

                startActivity(intent);
            }
        });


        validateRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addUser(mail.getText().toString(), password.getText().toString());
            }
        });

        //On surveille les radio_button
        group_sexe.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.inscription_radio_h:
                        sexe = "Homme"; //on retourne
                        break;
                    case R.id.inscription_radio_f:
                        sexe = "Femme";
                        break;
                }
            }
        });

        //Date
        openCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

    }

    //méthode qui affiche la pop-up pour choisir la date de naissance
    public void showDatePickerDialog(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.show();
    }

    //Méthode qui définit la date de naissance + calcul l'âge de l'utilisateur
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth){
        date = (month+1) + "/" + dayOfMonth + "/" + year; //format américain : mois/jours/année -- month+1 car commence à 0
        date_result.setText(date);



        //calcul de l'âge
        Calendar today = Calendar.getInstance();
        Calendar user_calendar = Calendar.getInstance();
        user_calendar.set(year,month,dayOfMonth);

        //année courante - année de naissance de l'utilisateur
        age = today.get(Calendar.YEAR) - year;

        //Si mois de l'utilisateur supérieur à celui d'aujourd'hui alors anniversaire pas atteint
        if(month > today.get(Calendar.MONTH)){
            age--;
        }else if(month == today.get(Calendar.MONTH)){
            //Si le jour est inférieur à celui de l'utilisateur alors anniversaire pas atteint
            if(today.get(Calendar.DAY_OF_MONTH) < dayOfMonth){
                age--;
            }
        }

    }

    //méthode qui ajoute un utilisateur dans notre base de données
    public void addUser(String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            FirebaseUser user = mAuth.getCurrentUser();
                            //Si tout s'est bien passé -> redirige vers la page de connexion
                            addData(mail.getText().toString(), mAuth.getUid());

                            Log.d(TAG, "createUserWithEmail:success");
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(InscriptionActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    //méthode qui initialise nos autres champs (Firestore) dans notre base de données
    public void addData(String email, String id){
        Map<String, Object> user = new HashMap<>();
        user.put("Prénom", firstName.getText().toString());
        user.put("Nom", name.getText().toString());
        user.put("email", email);
        user.put("date_naissance", date);
        user.put("age", age);
        user.put("sexe", sexe);
        user.put("rang",0);
        user.put("score", 0); //0 à la création d'un utilisateur

        db.collection("users").document(id)
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Intent intent = new Intent(InscriptionActivity.this, ConnexionActivity.class);
                        startActivity(intent);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });
    }

}