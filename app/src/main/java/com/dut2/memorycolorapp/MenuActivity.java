package com.dut2.memorycolorapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dut2.memorycolorapp.niveaux.Niveau1;
import com.dut2.memorycolorapp.niveaux.Niveau6;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;


public class MenuActivity extends AppCompatActivity {

    TextView username;
    TextView user_points;
    TextView user_rang;
    TextView test;

    ImageButton easy;
    ImageButton hard;
    ImageButton expert;
    ImageButton chrono;
    ImageButton infos;

    private FirebaseAuth mAuth;
    private DocumentReference docRef;
    private CollectionReference colRef;
    private FirebaseFirestore db;
    String TAG="BDDInfo";


    private Bundle bundle;
    private double points;
    private int rang;
    private List<String> topPlayers; // liste qui contiendra tous les joueurs devant l'utilisateur dans le classement

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        username = findViewById(R.id.menu_username);
        user_points = findViewById(R.id.menu_points);
        user_rang = findViewById(R.id.menu_rang);

        easy = findViewById(R.id.menu_btn_easy);
        hard = findViewById(R.id.menu_btn_hard);
        expert = findViewById(R.id.menu_btn_expert);
        chrono = findViewById(R.id.menu_btn_chrono);
        infos = findViewById(R.id.menu_btn_infos);

        test=findViewById(R.id.test);

        //Récupération de notre instance firebase
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();


        //Accès à la page d'informations
        infos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_info = new Intent(MenuActivity.this, InformationsActivity.class);
                startActivity(intent_info);
            }
        });


        //Clicks listener sur les boutons pour accéder aux modes
        easy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, Niveau1.class); //redirection vers le premier niveau
                bundle = new Bundle();

                bundle.putString("name_mode","FACILE");
                bundle.putDouble("weight", 1);
                bundle.putInt("nb_bloc_start", 1);
                bundle.putInt("max_bloc", 1);
                bundle.putInt("max_life", 2);

                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        hard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, Niveau1.class); //redirection vers le premier niveau
                bundle = new Bundle();

                bundle.putString("name_mode","DIFFICILE");
                bundle.putDouble("weight", 1.5);
                bundle.putInt("nb_bloc_start", 3);
                bundle.putInt("max_bloc", 15);
                bundle.putInt("max_life", 2);

                intent.putExtras(bundle);

                startActivity(intent);
            }
        });
        expert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, Niveau1.class); //redirection vers le premier niveau

                bundle = new Bundle();
                bundle.putString("name_mode","EXPERT");
                bundle.putDouble("weight", 3);
                bundle.putInt("nb_bloc_start", 5);
                bundle.putInt("max_bloc", 20);
                bundle.putInt("max_life", 3);

                intent.putExtras(bundle);

                startActivity(intent);
            }
        });

        //Appel des données à la création de l'activité
        getData();

    }

    //permet l'actualisation du score lors du RETOUR sur l'activité Menu
    @Override
    protected void onResume() {
        super.onResume();

        //Appel des données
        getData();

    }


    //Méthode qui recupère en base de données les données requises
    private void getData(){
        //Récupération des données en BDD

        docRef = db.collection("users").document(mAuth.getUid());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        username.setText(document.get("Nom").toString() + " " + document.get("Prénom").toString());
                        points = document.getDouble("score");
                        user_points.setText(points + " pts");

                        ranking(points);

                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });

    }


    //Méthode qui permet de calculer le rang d'un utilisateur
    private void ranking(double points){
        db.collection("users")
                .whereGreaterThan("score", points)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {
                            topPlayers = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                topPlayers.add(document.getString("Nom")); //on ajoute dans une liste le nombre de joueur devant l'utilisateur

                            }
                            user_rang.setText("Rang : " + (topPlayers.size()+1));
                            docRef.update("rang", (topPlayers.size()+1)); //on met à jour le classement du joueur
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }

                });
            }
    }

