package com.dut2.memorycolorapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dut2.memorycolorapp.niveaux.Niveau1;

public class PerduActivity extends AppCompatActivity {


    ImageButton restart;
    ImageButton home;
    TextView mode;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perdu);

        restart = findViewById(R.id.btn_restart);
        home = findViewById(R.id.btn_home);
        mode = findViewById(R.id.lose_mode);
        bundle = new Bundle();

        Intent current_intent = getIntent(); //on récupère l'instance de l'intent qui a démarré cette activité
        mode.setText("Mode essayé : " + current_intent.getStringExtra("mode"));


        restart.setOnClickListener(new View.OnClickListener() {
            //On relance l'activité avec les bundle correspondant : selon les modes
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PerduActivity.this, Niveau1.class);

                switch (current_intent.getStringExtra("mode")){
                    case "FACILE":
                        bundle.putString("name_mode","FACILE");
                        bundle.putDouble("weight", 1);
                        bundle.putInt("nb_bloc_start", 1);
                        bundle.putInt("max_bloc", 10);
                        bundle.putInt("max_life", 2);
                        intent.putExtras(bundle);

                        break;

                    case "DIFFICILE":
                        bundle.putString("name_mode","DIFFICILE");
                        bundle.putDouble("weight", 1.5);
                        bundle.putInt("nb_bloc_start", 3);
                        bundle.putInt("max_bloc", 15);
                        bundle.putInt("max_life", 2);
                        intent.putExtras(bundle);

                        break;

                    case "EXPERT":
                        bundle.putString("name_mode","EXPERT");
                        bundle.putDouble("weight", 3);
                        bundle.putInt("nb_bloc_start", 5);
                        bundle.putInt("max_bloc", 20);
                        bundle.putInt("max_life", 3);
                        intent.putExtras(bundle);

                        break;

                    default: //par défaut : on redirige vers le mode facile
                        bundle.putString("name_mode","FACILE");
                        bundle.putDouble("weight", 1);
                        bundle.putInt("nb_bloc_start", 1);
                        bundle.putInt("max_bloc", 10);
                        bundle.putInt("max_life", 2);
                        intent.putExtras(bundle);

                        break;
                }
                startActivity(intent);
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PerduActivity.this, MenuActivity.class);
                startActivity(intent);
            }
        });

    }
}