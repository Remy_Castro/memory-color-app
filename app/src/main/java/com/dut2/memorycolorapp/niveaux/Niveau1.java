package com.dut2.memorycolorapp.niveaux;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.res.ColorStateList;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dut2.memorycolorapp.PerduActivity;
import com.dut2.memorycolorapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static com.dut2.memorycolorapp.R.*;

public class Niveau1 extends AppCompatActivity {
    private int nbBloc_sequence; //Nombre de séquence réussie du joueur, par défaut 1 pour 1 bloc
    private List<Integer> sequence; //séquence à reussir
    private List<Integer> correct_sequence;
    private int index; //nombre de blocs validés dans une séquence
    private int MaxSequence; //nombre de séquence maximale pour le niveau 1
    private double poids_mode;
    private double user_points; //points de l'utilisateur
    private int level;
    private double won_points; //point à gagner
    private int health; //vie restante
    private Timer timer;
    private TimerTask task;
    private Bundle bundle;

    Button red,green,blue,yellow;
    TextView level_x; //Montre le niveau dans lequel l'utilisateur est
    TextView text_nb_sequence; //Montre la nombre de séquence réussie
    ImageView life_1;
    ImageView life_2;
    ImageView life_3;

    //BDD
    private FirebaseAuth mAuth;
    private DocumentReference docRef;
    private FirebaseFirestore db;
    String TAG="BDDInfo";


    private View.OnClickListener redClick;
    private View.OnClickListener greenClick;
    private View.OnClickListener blueClick;
    private View.OnClickListener yellowClick;

    private int count; //compteur qui permet de savoir si une séquence est terminée

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_niveau1);

        //intialisation des variables de configurations
        //récupération du bundle
        Intent intent = getIntent();
        bundle = intent.getExtras();

        index = 0;
        health = bundle.getInt("max_life");
        level = 1;
        correct_sequence = new ArrayList<>();
        MaxSequence = bundle.getInt("max_bloc");
        nbBloc_sequence = bundle.getInt("nb_bloc_start");
        poids_mode = bundle.getDouble("weight");
        won_points = level*poids_mode;
        count = 0;


        //Récupération de notre instance firebase
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        docRef = db.collection("users").document(mAuth.getUid());



        //Initialisation des boutons
        red = findViewById(id.btn_red_1);
        green = findViewById(id.btn_green_1);
        blue = findViewById(id.btn_blue_1);
        yellow = findViewById(id.btn_yellow_1);
        life_1 = findViewById(id.img_life_1);
        life_2 = findViewById(id.img_life_2);
        life_3 = findViewById(id.img_life_3);

        level_x = findViewById(id.textView_niveau);
        text_nb_sequence = findViewById(id.textView_sequence);


        //Affichage des vies : Si 2 vies : on affiche pas la troisième
        if(bundle.getInt("max_life") == 2){
            life_3.setVisibility(View.GONE);
        }

        //Initialisation des textview
        level_x.setText("Niveau : " + level + " - " + bundle.getString("name_mode"));

        //initialisation des couleurs
        setColors(red,green,blue,yellow);

        Toast.makeText(getApplicationContext(), "Bienvenue dans le niveau 1 de Color Memory", Toast.LENGTH_SHORT).show();

        //Définition de la séquence
        sequence = new ArrayList<>();
        correct_sequence = new ArrayList<>();
        sequence = initSequence(nbBloc_sequence);
        correct_sequence = sequence;

        //Initialisation des évenements de clicks
        redClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(checkSequence(correct_sequence.get(index), id.btn_red_1) ){ // À chaque clic, on vérifie si le bloc est bon à l'index correspondant
                        try {
                            isSequenceDone(correct_sequence);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        greenClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(checkSequence(correct_sequence.get(index), id.btn_green_1) ){
                        try {
                            isSequenceDone(correct_sequence);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        blueClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(checkSequence(correct_sequence.get(index), id.btn_blue_1) ){
                        try {
                            isSequenceDone(correct_sequence);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        yellowClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(checkSequence(correct_sequence.get(index), id.btn_yellow_1) ){
                        try {
                            isSequenceDone(correct_sequence);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        };


        red.setOnClickListener(redClick);
        green.setOnClickListener(greenClick);
        blue.setOnClickListener(blueClick);
        yellow.setOnClickListener(yellowClick);

        //Par défaut pas cliquable
        red.setClickable(false);
        green.setClickable(false);
        blue.setClickable(false);
        yellow.setClickable(false);

    }


    //Initialisation des couleurs des blocs
    public void setColors(Button r, Button g, Button b, Button y){
        r.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.btn_red)));
        g.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.btn_green)));
        b.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.btn_blue)));
        y.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.btn_yellow)));
    }



    //Défini une séquence, taille définie par sequence_achieved qui augmente selon séquence réussie
    public List<Integer> initSequence(int sequence_achieved) {

        List<Integer> sequence = new ArrayList<>();
        for (int i = 0; i < sequence_achieved ; i++){
            int id_random = getIdRandomBlock();
            sequence.add(id_random);
        }
        startSequence(sequence, sequence_achieved);
        //On ajoute dans la liste les ids des blocs dans un ordre aléatoire définit par la fonction getIdRandomBlock()
        return sequence;

    }


    //Méthode qui démarre l'affichage de la séquence à l'utilisateur
    public void startSequence(List<Integer> sequence, int sequence_achieved){
        timer = new Timer();

        correct_sequence = sequence;

        //Désactivation de la détection du clique des boutons

        red.setClickable(false);
        green.setClickable(false);
        blue.setClickable(false);
        yellow.setClickable(false);

        text_nb_sequence.setText("Séquence : " + String.valueOf(sequence.size()));

        //Utilisation d'un timer pour la gestion du délai
        task = new TimerTask() {
            @Override
            public void run() {
                for (int i = 0; i < sequence_achieved; i++) {
                    count++; //permet de compter le nombre de bloc éclairé -> afin de connaitre la fin de l'affichage de la séquence

                    //clique désactivé pendant la démonstration de séquence
                    red.setClickable(false);
                    green.setClickable(false);
                    blue.setClickable(false);
                    yellow.setClickable(false);

                    try {
                        illuminateBlock(sequence.get(i));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    //Affichage de la séquence terminée
                    if (count == nbBloc_sequence) {
                        count=0;
                        //permet d'utiliser un Toast (composant UI) dans notre thread : affichage à la fin de la tâche
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "À vous ! ", Toast.LENGTH_SHORT).show();
                            }
                        });
                        red.setClickable(true);
                        green.setClickable(true);
                        blue.setClickable(true);
                        yellow.setClickable(true);
                    }
                }
            }
        };
        timer.schedule(task, 3000);

    }


    //vérifie si le bloc cliqué est correct en comparant les ids
    public boolean checkSequence(int correct_block, int user_block) throws InterruptedException {
        if(correct_block == user_block){ //le block correspond
            index++;
            return true;
        }else{
            index = 0;
            health--;

            if(health == 2){ //l'utilisateur n'a plus que deux vies
                life_3.setVisibility(View.GONE);
            }else if(health == 1){
                life_2.setVisibility(View.GONE);
            }else{ //L'utilisateur n'a plus de vie -> il est rédirigé vers une page ou il pourra choisir de recommencer ou non
                life_1.setVisibility(View.GONE);

                //On redirige l'utilisateur vers la page d'echec
                Intent intent_lose = new Intent(Niveau1.this, PerduActivity.class);
                intent_lose.putExtra("mode",bundle.getString("name_mode"));
                startActivity(intent_lose);
            }

            //l'utilisateur s'est trompé mais a encore une/des vie(s) -> il recommence la même séquence
            if (health!=0){
                Toast.makeText(getApplicationContext(), "Erreur : il vous reste " + health + " vie(s)", Toast.LENGTH_SHORT).show();
                //on relance la séquence
                startSequence(correct_sequence, nbBloc_sequence);
            }
            return false;
        }
    }

    //Recupère une ID sur un des quatres blocs
    public int getIdRandomBlock(){
        List<Integer> ids = new ArrayList<>();

        ids.add(red.getId());
        ids.add(green.getId());
        ids.add(blue.getId());
        ids.add(yellow.getId());

        Random rand = new Random();
        int randomIndex = ids.get(rand.nextInt(ids.size()));

        return randomIndex;
    }

    //Vérifie si la séquence est terminée en comparant l'index (nombre de blocs corrects dans la séquence) et la taille de la séquence
    public void isSequenceDone(List<Integer> sequence) throws InterruptedException {
        if(index == sequence.size()){

            Toast.makeText(getApplicationContext(), "Vous avez réussi la séquence " + index + "/"+MaxSequence, Toast.LENGTH_SHORT).show();

            //L'utilisateur a terminé le premier niveau quand il a fait 6 séquences
            if(index == MaxSequence){
                Toast.makeText(getApplicationContext(), "Vous avez terminé le premier niveau ! Bravo ", Toast.LENGTH_SHORT).show();

                //mise à jour des points en BDD
                updateUserPoints();

                //redirection vers le prochain niveau
                Intent intent = new Intent(Niveau1.this, Niveau2.class);
                intent.putExtras(bundle);
                startActivity(intent);

            }else{
                index = 0; //on réinitialise index
                nbBloc_sequence++; //on incrémente le nombre de blocs
                count = 0;

                //On ajoute un nouveau bloc à la séquence
                correct_sequence.add(getIdRandomBlock());
                startSequence(correct_sequence, nbBloc_sequence);
            }
        }
    }

    public void illuminateBlock(int id_block) throws InterruptedException {
        Button btn_illuminate = findViewById(id_block);

        btn_illuminate.getBackground().setAlpha(45);
        Thread.sleep(500); //temps d'illumination
        btn_illuminate.getBackground().setAlpha(255);
        Thread.sleep(500); //temps de pause entre l'illumination de chaque blocs

    }


    public void updateUserPoints(){
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        user_points = document.getDouble("score");
                        docRef.update("score", user_points+won_points);

                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });

    }



}