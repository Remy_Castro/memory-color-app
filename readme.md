# Application ColorMemory : projet DUT Informatique 

La réalisation de ce jeu a été faite dans le cadre du projet de deuxième année de DUT Informatique 2021.

## I. Base de données

### Outil : Firebase

Pour ce projet nous avons choisi d'utiliser Firebase et notamment le système d'authenfication avec email/mot de passe. <br/> Ainsi que le cloud
firestore pour stocker les autres champs (Nom, prénom, age, date de naissance, email, score, le rang et le sexe)


### Gestion des données

#### La mise à jours des points 

Lorsqu'un joueur termine un niveau, une fonction updateUserPoints() est appelé et met à jour les points.
À partir de la référence de notre document Firebase, il est possible de récupérer les données et de les modifier. 

Ainsi, pour mettre à jour les données il suffit de :  

```jshelllanguage
docRef.update("score", user_points+won_points);
```
 Où user_points et les points actuels du joueur, et won_points est calculé par niveau * poids du mode

#### Classement des joueurs

Pour déterminer le classement des joueurs, nous avons crée une fonction dans le menu qui récupère en base de données la liste des ids de joueurs qui ont un score **strictement** supérieur à celui du joueur connecté. Cette liste est ensuite compté et incrementé de 1 pour obtenir la place du joueur.


## II. Les modes

Dans ColorMemory, les modes permettent de choisir la difficulté (facile, difficile, expert)

Le mode facile : démarre de 1 bloc à éclairer, jusqu'à 10 (Poids : 1)
Le mode difficile : démarre de 3 blocs à éclairer, jusqu'à 15 (Poids 1.5)
Le mode expert : démarre de 5 blocs à éclairer, jusqu'à 20 (Poids 3)

### Gestion des modes 

Pour ne pas surcharger l'application d'activités, nous avons choisi de transférer des données depuis le menu vers 
le niveau 1 via des extras. 

Pour cela nous avons utilisé un bundle, qui contient des données différentes selon le mode choisi.

#### Exemple pour le mode difficile

- Transfère des données

```
bundle = new Bundle();
bundle.putString("name_mode","DIFFICILE");
bundle.putDouble("weight", 1.5);
bundle.putInt("nb_bloc_start", 3);
bundle.putInt("max_bloc", 15);
bundle.putInt("max_life", 2);
```

- Réception des données dans le niveau 1

```jshelllanguage
health = bundle.getInt("max_life");
MaxSequence = bundle.getInt("max_bloc");
nbBloc_sequence = bundle.getInt("nb_bloc_start");
poids_mode = bundle.getDouble("weight");
```

Ce système nous permet de pouvoir ajouter de nouveaux modes facilement. 

## III. Les niveaux

Le jeu demandé comporte sept niveaux faisant varier le nombre de blocs à l'écran. <br/> Soit de quatre blocs pour le niveau 1 à dix blocs pour le niveau 2 

### Gestion de la séquence

#### Blocs aléatoires

Nous avons défini une fonction qui ajoute retourne l'ID d'un bloc pris aléatoirement dans une liste contenant tous les blocs grâce à la fonction random()
Au lancement d'un niveau, une séquence est générée (liste contenant <nbBlocs>) : ajoute dans une liste la liste des ids 

```jshelllanguage
randomIndex = ids.get(rand.nextInt(ids.size()));
```

#### Création de la séquence

Une fonction prenant en paramètre le nombre de blocs à éclairer durant cette séquence permet cette création. 
En effet, elle ajoute dans une liste, les IDs des blocs générés par notre fonction aléatoire. 

```jshelllanguage
List<Integer> sequence = new ArrayList<>();
        for (int i = 0; i < sequence_achieved ; i++){
            int id_random = getIdRandomBlock();
            sequence.add(id_random);
        }
```
 

#### Affichage de la séquence

Pour afficher la séquence, on a défini une autre fonction prenant en paramètre la séquence à afficher puis le nombre de blocs. 
<br/>Ainsi, pour chaque bloc dans la séquence, on fait appel à une fonction modifiant l'opacité d'un bloc.  

Également, nous avons fait le choix d'utiliser un Timer() pour mettre en place un délai avant d'exécuter notre fonction allumant les blocs

#### Vérification de la correspondance des blocs

Pour vérifier si l'utilisateur a cliqué sur le bon bloc, une fonction checkSequence() a été créée. Celle ci vérifie la correspondance entre l'id de la séquence à trouver, et l'id du bloc cliqué. <br/>
Ainsi, il est possible de déterminer à chaque clique, si le bloc est correct. Si ce n'est pas correct l'affichage de la séquence est relancé et le joueur perd 1 vie, s'il en a plus, il est rédirigé vers une page d'échec de partie.

#### Fin de la séquence

Pour vérifier si une séquence est terminée, on fait appel à une fonction qui compare l'index du bloc confirmé (la position du bloc correct dans la séquence) à la taille de la séquence. <br/>
Ainsi, si l'index du bloc confirmé est égal à la taille de la liste de séquence alors le joueur a terminé la séquence -> la séquence est relancé avec un bloc aléatoire en plus.

Si index (le bloc qui a été confirmé) est égal au nombre maximal de blocs que peut contenir une séquence pour un niveau, alors le joueur a terminé le niveau, il est redirigé vers le niveau supérieur. 

### Génération aléatoire de la couleur des blocs

Lorsque nous passons à un niveau supérieur, un bloc est ajouté. Pour permettre à ce bloc d'adopté une couleur aléatoire,
on a défini un tableau de couleurs dans le fichier app/res/values/colors.xml. Nous avons alors stocké dans un tableau la liste des couleurs.

Pour pouvoir choisir aléatoirement une couleur sans reprendre la même plusieurs fois, nous avons eu l'idée de mélanger le tableau. 
Pour cela, nous devions avoir une liste non pas un tableau, pour cela, nous devions avoir un tableau de Integer[] au lieu de int[]. 
<br/> Nous avons donc crée une méthode pour convertir un tableau int[] en Integer[].

```jshelllanguage
 public void convertArray(int[] old_array){
        tabColorsConverted = new Integer[old_array.length];
        for(int i = 0 ; i < old_array.length; i++){
            tabColorsConverted[i] = Integer.valueOf(old_array[i]);
        }
    }
```

Avec un tableau d'Integer, il est possible d'accéder à la fonction Arrays.asList(Integer[]) afin de pouvoir utilisation Collections.shuffle(List).

## Built With

* [Android Studio](https://developer.android.com/studio) - Integrated development environment for Google's Android operating system

## Authors

* **Rémy CASTRO**  - [@Remy_Castro](https://gitlab.com/Remy_Castro)
* **Lucas MAROT**  - [@Lucas_Marot](https://gitlab.com/Lucas_Marot)
